# HorizonAPI



## Introduction

A RESTful API which handles animal and feeding visits records, developed using the Repository-Service Pattern.

## Technologies


1. **.NET 5**
> This project is developed using C# programming language on Microsoft's .NET 5 Framework.

2. **Entity Framework Core**
> Database operations are performed using Entity Framework Core 5.

3. **SQLite**
> The datas of the application is kept in SQLite, which is open source.

4. **Swagger**
> In this project Swagger is used as an Interface Description Language for describing RESTful APIs expressed using JSON.

## Run App

- Visual Studio installed on the Windows OS is needed to launch the application.

- After cloning the application's files, the steps to be followed are as follows:
> Visual Studio > Tools > Nuget Package Manager > Package Manager Console
```html
Add-Migration InitialCreate
```
```html
Update-Database
```

- That's all. Click on IIS Express 👏
