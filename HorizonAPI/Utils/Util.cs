﻿using System;
using System.Linq;
using System.Linq.Expressions;
using static Horizon.Common.Constants;

namespace Horizon.Utils
{
    internal class Util
    {
        public static string RaiseErrorMessage(int messageType)
        {
            var errorMessage = string.Empty;
            switch (messageType)
            {
                case (int)MessageType.NotExists:
                    errorMessage = "with given Id does not exist!";
                    break;
                case (int)MessageType.AlreadyExists:
                    errorMessage = "with given Id already exists!";
                    break;
                default:
                    break;
            }

            return errorMessage;
        }

        internal static Expression<Func<T, bool>> CombineExpressions<T>(Expression<Func<T, bool>> firstExpression, Expression<Func<T, bool>> secondExpression)
        {
            var invokedExpression = Expression.Invoke(secondExpression, firstExpression.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(firstExpression.Body, invokedExpression), firstExpression.Parameters);
        }
    }
}
