﻿using System;
using Horizon.DTOs;
using Horizon.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Horizon.Services.Interfaces
{
    public interface IFeedVisitService
    {
        public Task<IEnumerable<FeedVisitDTO>> GetAll(
            Guid? AnimalId,
            DateTime? FeedingDate,
            int? Intake,
            int PageNumber = 1,
            int Limit = 25
            );

        public Task<FeedVisitDTO?> GetById(Guid id);

        public Task<FeedVisitDTO> Insert(FeedVisit feedVisit);

        public Task<FeedVisitDTO> Update(Guid id, FeedVisit feedVisit);

        public Task<FeedVisit> Delete(Guid id);
    }
}
