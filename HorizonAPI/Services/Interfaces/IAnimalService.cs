﻿using System;
using Horizon.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Horizon.Common.Constants;

namespace Horizon.Services.Interfaces
{
    public interface IAnimalService
    {
        public Task<IEnumerable<Animal>> GetAll(
            string? LifeNumber,
            string? Name,
            AnimalGender? Gender,
            DateTime? BirthDate,
            string? FatherLifeNumber,
            string? MotherLifeNumber,
            string? Description,
            int PageNumber = 1,
            int Limit = 25
            );

        public Task<Animal?> GetById(Guid id);

        public Task<Animal> Insert(Animal animal);

        public Task<Animal> Update(Guid id, Animal animal);

        public Task<Animal> Delete(Guid id);
    }
}
