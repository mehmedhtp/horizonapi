﻿using System;
using Horizon.DTOs;
using Horizon.Entities;
using Horizon.Repositories;
using Horizon.Services.Interfaces;
using Horizon.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Horizon.Services
{
    public class FeedVisitService : IFeedVisitService
    {
        private readonly IRepository<FeedVisit> _feedVisitRepository;
        private readonly IRepository<Animal> _animalRepository;

        public FeedVisitService(IRepository<FeedVisit> feedVisitRepository, IRepository<Animal> animalRepository)
        {
            this._feedVisitRepository = feedVisitRepository;
            this._animalRepository = animalRepository;
        }

        public async Task<IEnumerable<FeedVisitDTO>> GetAll(
            Guid? AnimalId,
            DateTime? FeedingDate,
            int? Intake,
            int PageNumber = 1,
            int Limit = 25
            )
        {
            Expression<Func<FeedVisit, bool>> filter = a => true;
            if (AnimalId is not null)
            {
                Expression<Func<FeedVisit, bool>> expression = v => v.AnimalId == AnimalId;
                filter = Util.CombineExpressions(filter, expression);
            }

            if (FeedingDate is not null)
            {
                Expression<Func<FeedVisit, bool>> expression = v => v.FeedingDate == FeedingDate;
                filter = Util.CombineExpressions(filter, expression);
            }

            if (Intake is not null)
            {
                Expression<Func<FeedVisit, bool>> expression = v => v.Intake == Intake;
                filter = Util.CombineExpressions(filter, expression);
            }

            IEnumerable<FeedVisit> feedVisits = await this._feedVisitRepository.GetAll(filter, x => x.OrderBy(s => s.FeedingDate), pageNumber: PageNumber, limit: Limit);
            IEnumerable<FeedVisitDTO> feedVisitsOutput = new List<FeedVisitDTO>();
            foreach (FeedVisit feedVisit in feedVisits)
                feedVisitsOutput = feedVisitsOutput.Append(new FeedVisitDTO(feedVisit, await this._animalRepository.GetById(feedVisit.AnimalId)));


            return feedVisitsOutput;
        }

        public async Task<FeedVisitDTO?> GetById(Guid id)
        {
            FeedVisit? feedVisit = await this._feedVisitRepository.GetById(id);
            if (feedVisit is null)
                return null;

            return new FeedVisitDTO(feedVisit, await this._animalRepository.GetById(feedVisit.AnimalId));
        }

        public async Task<FeedVisitDTO> Insert(FeedVisit feedVisit)
        {
            FeedVisit insertedFeedVisit = await this._feedVisitRepository.Insert(feedVisit);

            return new FeedVisitDTO(insertedFeedVisit, await this._animalRepository.GetById(insertedFeedVisit.AnimalId));
        }

        public async Task<FeedVisitDTO> Update(Guid id, FeedVisit feedVisit)
        {
            feedVisit.DeviceId = id;
            FeedVisit updatedFeedVisit = await this._feedVisitRepository.Update(feedVisit);

            return new FeedVisitDTO(updatedFeedVisit, await this._animalRepository.GetById(updatedFeedVisit.AnimalId));
        }

        public async Task<FeedVisit> Delete(Guid id) {
            return await this._feedVisitRepository.Delete(id);
        } 
    }
}
