﻿using System;
using Horizon.Entities;
using Horizon.Repositories;
using Horizon.Services.Interfaces;
using Horizon.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static Horizon.Common.Constants;

namespace Horizon.Services
{
    public class AnimalService : IAnimalService
    {
        private readonly IRepository<Animal> _animalRepository;

        public AnimalService(IRepository<Animal> animalRepository)
        {
            this._animalRepository = animalRepository;
        }

        public async Task<IEnumerable<Animal>> GetAll(
            string? LifeNumber,
            string? Name,
            AnimalGender? Gender,
            DateTime? BirthDate,
            string? FatherLifeNumber,
            string? MotherLifeNumber,
            string? Description,
            int PageNumber = 1,
            int Limit = 25
            )
        {
            Expression<Func<Animal, bool>> filter = a => true;
            if (!string.IsNullOrEmpty(LifeNumber))
            {
                Expression<Func<Animal, bool>> expression = a => a.LifeNumber == LifeNumber;
                filter = Util.CombineExpressions(filter, expression);
            }

            if (!string.IsNullOrEmpty(Name))
            {
                Expression<Func<Animal, bool>> expression = a => a.Name == Name;
                filter = Util.CombineExpressions(filter, expression);
            }

            if (Gender is not null)
            {
                Expression<Func<Animal, bool>> expression = a => a.Gender == Gender;
                filter = Util.CombineExpressions(filter, expression);
            }

            if (BirthDate is not null)
            {
                Expression<Func<Animal, bool>> expression = a => a.BirthDate == BirthDate;
                filter = Util.CombineExpressions(filter, expression);
            }

            if (!string.IsNullOrEmpty(FatherLifeNumber))
            {
                Expression<Func<Animal, bool>> expression = a => a.FatherLifeNumber == FatherLifeNumber;
                filter = Util.CombineExpressions(filter, expression);
            }

            if (!string.IsNullOrEmpty(MotherLifeNumber))
            {
                Expression<Func<Animal, bool>> expression = a => a.MotherLifeNumber == MotherLifeNumber;
                filter = Util.CombineExpressions(filter, expression);
            }

            if (!string.IsNullOrEmpty(Description))
            {
                Expression<Func<Animal, bool>> expression = a => a.Description == Description;
                filter = Util.CombineExpressions(filter, expression);
            }

            return await this._animalRepository.GetAll(filter, x => x.OrderBy(s => s.BirthDate), limit: Limit, pageNumber: PageNumber);
        }

        public async Task<Animal?> GetById(Guid id) => await this._animalRepository.GetById(id);

        public async Task<Animal> Insert(Animal animal) => await this._animalRepository.Insert(animal);

        public async Task<Animal> Update(Guid id, Animal animal)
        {
            animal.Id = id;
            return await this._animalRepository.Update(animal);
        }

        public async Task<Animal> Delete(Guid id)
        {
            return await this._animalRepository.Delete(id);
        }
    }
}
