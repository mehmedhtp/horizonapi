﻿using System;
using Horizon.Entities;
using Microsoft.EntityFrameworkCore;

namespace Horizon.Models
{
    public class HorizonDbContext : DbContext
    {
        public DbSet<Animal>? Animals { get; set; }
        public DbSet<FeedVisit>? FeedVisits { get; set; }

        public string DbPath { get; }

        public HorizonDbContext()
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = System.IO.Path.Join(path, "Horizon.db");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");
    }
}
