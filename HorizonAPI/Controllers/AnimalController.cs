﻿using System;
using Horizon.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Horizon.Services.Interfaces;
using Horizon.Utils;
using static Horizon.Common.Constants;

namespace Horizon.Controllers
{
    [ApiController]
    [Route("api/animals")]
    public class AnimalController : ControllerBase
    {
        private readonly IAnimalService _animalService;

        public AnimalController(IAnimalService animalService)
        {
            this._animalService = animalService;
        }

        [HttpGet]
        public async Task<object> GetAnimalsAsync(
            [FromQuery] string? LifeNumber,
            [FromQuery] string? Name,
            [FromQuery] AnimalGender? Gender,
            [FromQuery] DateTime? BirthDate,
            [FromQuery] string? FatherLifeNumber,
            [FromQuery] string? MotherLifeNumber,
            [FromQuery] string? Description,
            [FromQuery] int PageNumber = 1,
            [FromQuery] int Limit = 25
            )
        {
            if (PageNumber < 1)
                return BadRequest("PageNumber cannot smaller than 1!");

            if (Limit < 1)
                return BadRequest("Limit cannot smaller than 1!");

            if (Limit > 100)
                return BadRequest("Limit cannot greater than 100!");

            return await this._animalService.GetAll(LifeNumber, Name, Gender, BirthDate, FatherLifeNumber, MotherLifeNumber, Description, PageNumber, Limit);
        }

        [HttpGet("{id}")]
        public async Task<object> GetAnimalAsync([FromRoute] Guid id) 
        {
            Animal? animal = await this._animalService.GetById(id);
            if (animal is null)
                return BadRequest(string.Format("{0}{1}{2}", "Animal", string.Empty, Util.RaiseErrorMessage((int)MessageType.NotExists)));

            return animal;
        }

        [HttpPost]
        public async Task<object> InsertAnimalAsync([FromBody] Animal animal)
        {
            Animal existingAnimal = await this._animalService.GetById(animal.Id);
            if (existingAnimal is not null)
                return BadRequest(string.Format("{0}{1}{2}", "Animal", string.Empty, Util.RaiseErrorMessage((int)MessageType.AlreadyExists)));

            return await this._animalService.Insert(animal); 
        }

        [HttpPut("{id}")]
        public async Task<Animal> UpdateAnimalAsync([FromRoute] Guid id, [FromBody] Animal animal)
        {
            animal.Id = id;
            return await this._animalService.Update(id, animal);
        }

        [HttpDelete("{id}")]
        public async Task<object> DeleteAnimalAsync([FromRoute] Guid id)
        {
            Animal? animal = await this._animalService.GetById(id);
            if (animal is null)
                return BadRequest(string.Format("{0}{1}{2}", "Animal", string.Empty, Util.RaiseErrorMessage((int)MessageType.NotExists)));

            await this._animalService.Delete(id);
            return NoContent();
        }
    }
}
