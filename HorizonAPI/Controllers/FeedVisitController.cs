﻿using System;
using Horizon.DTOs;
using Horizon.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Horizon.Services.Interfaces;
using Horizon.Utils;
using static Horizon.Common.Constants;

namespace Horizon.Controllers
{
    [ApiController]
    [Route("api/feeding/visits")]
    public class FeedVisitController : ControllerBase
    {
        private readonly IFeedVisitService _feedVisitService;

        public FeedVisitController(IFeedVisitService feedVisitService)
        {
            this._feedVisitService = feedVisitService;
        }

        [HttpGet]
        public async Task<object> GetFeedVisitsAsync(
            [FromQuery] Guid? AnimalId,
            [FromQuery] DateTime? FeedingDate,
            [FromQuery] int? Intake,
            [FromQuery] int PageNumber = 1,
            [FromQuery] int Limit = 25
            )
        {
            if (PageNumber < 1)
                return BadRequest("PageNumber cannot smaller than 1!");

            if (Limit < 1)
                return BadRequest("Limit cannot smaller than 1!");

            if (Limit > 100)
                return BadRequest("Limit cannot greater than 100!");

            return await this._feedVisitService.GetAll(AnimalId, FeedingDate, Intake, PageNumber, Limit);
        }


        [HttpGet("{id}")]
        public async Task<object> GetFeedVisitAsync([FromRoute] Guid id)
        {
            FeedVisitDTO? feedVisit = await this._feedVisitService.GetById(id);
            if (feedVisit is null)
                return BadRequest(string.Format("{0}{1}{2}", "Feed Visit", string.Empty, Util.RaiseErrorMessage((int)MessageType.NotExists)));

            return feedVisit;
        }

        [HttpPost]
        public async Task<object> InsertFeedVisitAsync([FromBody] FeedVisit feedVisit)
        {
            FeedVisitDTO existingFeedVisit = await this._feedVisitService.GetById(feedVisit.DeviceId);
            if (existingFeedVisit is not null)
                return BadRequest(string.Format("{0}{1}{2}", "Feed Visit", string.Empty, Util.RaiseErrorMessage((int)MessageType.AlreadyExists)));

            return await this._feedVisitService.Insert(feedVisit);
        }

        [HttpPut("{id}")]
        public async Task<FeedVisitDTO> UpdateFeedVisitAsync([FromRoute] Guid id, [FromBody] FeedVisit feedVisit)
        {
            return await this._feedVisitService.Update(id, feedVisit);
        }

        [HttpDelete("{id}")]
        public async Task<object> DeleteFeedVisitAsync([FromRoute] Guid id)
        {
            FeedVisitDTO feedVisit = await this._feedVisitService.GetById(id);
            if (feedVisit is null)
                return BadRequest(string.Format("{0}{1}{2}", "Feed Visit", string.Empty, Util.RaiseErrorMessage((int)MessageType.NotExists)));

            await this._feedVisitService.Delete(id);
            return NoContent();
        }
    }
}
