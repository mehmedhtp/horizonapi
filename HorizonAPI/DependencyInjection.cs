﻿using Horizon.Entities;
using Horizon.Models;
using Horizon.Repositories;
using Horizon.Services;
using Horizon.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Horizon
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddScoped<IRepository<Animal>, BaseRepository<Animal>>();
            services.AddScoped<IRepository<FeedVisit>, BaseRepository<FeedVisit>>();

            return services;
        }

        public static IServiceCollection AddDbContext(this IServiceCollection services)
        {
            services.AddDbContext<HorizonDbContext>();

            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IAnimalService, AnimalService>();
            services.AddScoped<IFeedVisitService, FeedVisitService>();

            return services;
        }
    }
}
