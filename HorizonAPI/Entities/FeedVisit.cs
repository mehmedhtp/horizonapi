﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Horizon.Entities
{
    [Table("FeedVisit")]
    public class FeedVisit
    {
        [Key]
        [Column("DEVICE_ID")]
        [JsonPropertyName("DeviceId")]
        public Guid DeviceId { get; set; }

        [Column("ANIMAL_ID")]
        [JsonPropertyName("AnimalId")]
        public Guid AnimalId { get; set; }

        [Column("FEEDING_DATE")]
        [JsonPropertyName("FeedingDate")]
        public DateTime FeedingDate { get; set; }

        [Column("INTAKE")]
        [JsonPropertyName("Intake")]
        public int? Intake { get; set; }
    }
}
