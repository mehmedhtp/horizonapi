﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using static Horizon.Common.Constants;

namespace Horizon.Entities
{
    [Table("Animals")]
    public class Animal
    {
        [Key]
        [Column("ID")]
        [JsonPropertyName("Id")]
        public Guid Id { get; set; }

        [Column("LIFE_NUMBER")]
        [JsonPropertyName("LifeNumber")]
        public string? LifeNumber { get; set; }

        [Column("NAME")]
        [JsonPropertyName("Name")]
        public string? Name { get; set; }

        [Column("GENDER")]
        [JsonPropertyName("Gender")]
        public AnimalGender Gender { get; set; }

        [Column("BIRTH_DATE")]
        [JsonPropertyName("BirthDate")]
        public DateTime BirthDate { get; set; }

        [Column("FATHER_LIFE_NUMBER")]
        [JsonPropertyName("FatherLifeNumber")]
        public string? FatherLifeNumber { get; set; }

        [Column("MOTHER_LIFE_NUMBER")]
        [JsonPropertyName("MotherLifeNumber")]
        public string? MotherLifeNumber { get; set; }

        [Column("DESCRIPTION")]
        [JsonPropertyName("Description")]
        public string? Description { get; set; }
    }
}
