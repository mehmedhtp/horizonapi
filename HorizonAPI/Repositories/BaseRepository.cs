﻿using Horizon.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Horizon.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : class
    {
        public readonly HorizonDbContext context;
        public readonly DbSet<T> dbSet;

        public BaseRepository(HorizonDbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<T>();
        }

        public async virtual Task<IEnumerable<T>> GetAll(
            Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            string includeProperties = "",
            int pageNumber = 1,
            int limit = 25
            )
        {
            IQueryable<T> query = dbSet;
            if (filter is not null)
                query = query.Where(filter);

            var properties = includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var property in properties)
                query.Include(property);

            if (orderBy is null)
            {
                return await query.Skip((pageNumber - 1) * limit).Take(limit).ToListAsync();
            }
            else
            {
                return await orderBy(query).Skip((pageNumber - 1) * limit).Take(limit).ToListAsync();
            }
        }

        public async virtual Task<T> GetById(object id)
        {
            return await dbSet.FindAsync(id);
        }

        public async virtual Task<T> Insert(T entity)
        {
            var insertedEntity = dbSet.Add(entity).Entity;
            await context.SaveChangesAsync();

            return insertedEntity;
        }

        public async virtual Task<T> Update(T entity)
        {
            T updatedEntity = dbSet.Attach(entity).Entity;
            context.Entry(entity).State = EntityState.Modified;
            await context.SaveChangesAsync();

            return updatedEntity;
        }

        public async virtual Task<T> Delete(object id)
        {
            T? entitoToDelete = await GetById(id);
            if (entitoToDelete is not null)
            {
                Delete(entitoToDelete);
                await context.SaveChangesAsync();
            }

            return entitoToDelete;
        }

        public async virtual void Delete(T entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
                dbSet.Attach(entity);

            dbSet.Remove(entity);
            await context.SaveChangesAsync();
        }
    }
}
