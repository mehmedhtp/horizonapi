﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Horizon.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll(
            Expression<Func<T, bool>>? filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            string includeProperties = "",
            int pageNumber = 1,
            int limit = 25
            );

        Task<T> GetById(object id);

        Task<T> Insert(T entity);

        Task<T> Update(T entity);

        Task<T> Delete(object id);

        void Delete(T entity);
    }
}
