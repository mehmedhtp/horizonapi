﻿using Horizon.Entities;
using System;
using System.Text.Json.Serialization;

namespace Horizon.DTOs
{
    public class FeedVisitDTO
    {
        [JsonPropertyName("DeviceId")]
        public Guid DeviceId { get; set; }

        [JsonPropertyName("AnimalId")]
        public Guid AnimalId { get; set; }

        [JsonPropertyName("Animal")]
        public Animal? Animal { get; set; }

        [JsonPropertyName("FeedingDate")]
        public DateTime FeedingDate { get; set; }

        [JsonPropertyName("Intake")]
        public int? Intake { get; set; }

        public FeedVisitDTO(FeedVisit feedVisit, Animal? animal)
        {
            this.DeviceId = feedVisit.DeviceId;
            this.AnimalId = feedVisit.AnimalId;
            this.Animal = animal;
            this.FeedingDate = feedVisit.FeedingDate;
            this.Intake = feedVisit.Intake;
        }

        public override bool Equals(object? obj)
        {
            if (obj is null 
                || this.Animal is not null && !(this.Animal == ((FeedVisitDTO)obj).Animal)
                || this.Animal is null && ((FeedVisitDTO)obj).Animal is not null)
                return false;

            return this.DeviceId.Equals(((FeedVisitDTO)obj).DeviceId) 
                && this.AnimalId.Equals(((FeedVisitDTO)obj).AnimalId) 
                && this.FeedingDate.Equals(((FeedVisitDTO)obj).FeedingDate) 
                && this.Intake.Equals(((FeedVisitDTO)obj).Intake);
        }
    }
}
