﻿namespace Horizon.Common
{
    public class Constants
    {
        public enum AnimalGender
        {
            Male = 1,
            Female = 2,
            Other = 3
        }

        public enum MessageType
        {
            NotExists = 0,
            AlreadyExists = 1
        }
    }
}
